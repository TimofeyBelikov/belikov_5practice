def KelVCel(a):
 return a - 273.15

def KelVFah(a):
 return a*1.8-459.67

def CelVFah(a):
 return a * (9 / 5) + 32

def CelVKel(a):
 return a + 273.15

def FahVCel(a):
 return (a - 32) * 5 / 9

def FahVKel(a):
 return (a - 32) * (5 / 9) + 273.15

def CelVCel(a):
 return a

def KelVKel(a):
 return a

def FahVFah(a):
 return a

def transform(transform, origin_scale, second_scale):
 file = open("test.txt", "a")
 if (origin_scale != "C" and origin_scale != "F" and origin_scale != "K") or \
         (second_scale != "C" and second_scale != "F" and second_scale != "K"):
  file.write("The scale was entered incorrectly\n")
  return "The scale was entered incorrectly"
 if origin_scale == "C":
  if second_scale == "F":
   temp = CelVFah(transform)
  elif second_scale == "K":
   temp = CelVKel(transform)
  elif second_scale == "C":
   temp = CelVCel(transform)
 elif origin_scale == "K":
  if second_scale == "C":
    temp = KelVCel(transform)
  elif second_scale == "F":
    temp = KelVFah(transform)
  elif second_scale == "K":
    temp = KelVKel(transform)
 elif origin_scale == "F":
  if second_scale == "C":
    temp = FahVCel(transform)
  elif second_scale == "K":
    temp = FahVKel(transform)
  elif second_scale == "F":
    temp = FahVFah(transform)
 file.write(str(transform) + " " + origin_scale + " = " + str(temp) + " " + second_scale + "\n")
 return temp
